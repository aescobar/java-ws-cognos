
package com.fie.cognos.clientesin.clientehora;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para respuestaFechaHora complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="respuestaFechaHora">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechaHora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaCodigosRespuestas" type="{https://siat.impuestos.gob.bo/}respuestaCodigosMensajesSoapDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="transaccion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaFechaHora", propOrder = {
    "fechaHora",
    "listaCodigosRespuestas",
    "transaccion"
})
public class RespuestaFechaHora {

    protected String fechaHora;
    @XmlElement(nillable = true)
    protected List<RespuestaCodigosMensajesSoapDto> listaCodigosRespuestas;
    protected boolean transaccion;

    /**
     * Obtiene el valor de la propiedad fechaHora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHora() {
        return fechaHora;
    }

    /**
     * Define el valor de la propiedad fechaHora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHora(String value) {
        this.fechaHora = value;
    }

    /**
     * Gets the value of the listaCodigosRespuestas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCodigosRespuestas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCodigosRespuestas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaCodigosMensajesSoapDto }
     * 
     * 
     */
    public List<RespuestaCodigosMensajesSoapDto> getListaCodigosRespuestas() {
        if (listaCodigosRespuestas == null) {
            listaCodigosRespuestas = new ArrayList<RespuestaCodigosMensajesSoapDto>();
        }
        return this.listaCodigosRespuestas;
    }

    /**
     * Obtiene el valor de la propiedad transaccion.
     * 
     */
    public boolean isTransaccion() {
        return transaccion;
    }

    /**
     * Define el valor de la propiedad transaccion.
     * 
     */
    public void setTransaccion(boolean value) {
        this.transaccion = value;
    }

}
