
package com.fie.cognos.clientesin.cliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para respuestaCierreSistemas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="respuestaCierreSistemas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaCodigosRespuestas" type="{https://siat.impuestos.gob.bo/}respuestaCodigosMensajesSoapDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="transaccion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaCierreSistemas", propOrder = {
    "codigoSistema",
    "listaCodigosRespuestas",
    "transaccion"
})
public class RespuestaCierreSistemas {

    protected String codigoSistema;
    @XmlElement(nillable = true)
    protected List<RespuestaCodigosMensajesSoapDto> listaCodigosRespuestas;
    protected boolean transaccion;

    /**
     * Obtiene el valor de la propiedad codigoSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSistema() {
        return codigoSistema;
    }

    /**
     * Define el valor de la propiedad codigoSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSistema(String value) {
        this.codigoSistema = value;
    }

    /**
     * Gets the value of the listaCodigosRespuestas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCodigosRespuestas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCodigosRespuestas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaCodigosMensajesSoapDto }
     * 
     * 
     */
    public List<RespuestaCodigosMensajesSoapDto> getListaCodigosRespuestas() {
        if (listaCodigosRespuestas == null) {
            listaCodigosRespuestas = new ArrayList<RespuestaCodigosMensajesSoapDto>();
        }
        return this.listaCodigosRespuestas;
    }

    /**
     * Obtiene el valor de la propiedad transaccion.
     * 
     */
    public boolean isTransaccion() {
        return transaccion;
    }

    /**
     * Define el valor de la propiedad transaccion.
     * 
     */
    public void setTransaccion(boolean value) {
        this.transaccion = value;
    }

}
