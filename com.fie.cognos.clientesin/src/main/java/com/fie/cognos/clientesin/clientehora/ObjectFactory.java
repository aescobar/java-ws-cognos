
package com.fie.cognos.clientesin.clientehora;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.clientesin.clientehora package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerificarComunicacion_QNAME = new QName("https://siat.impuestos.gob.bo/", "verificarComunicacion");
    private final static QName _VerificarComunicacionResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "verificarComunicacionResponse");
    private final static QName _SincronizarFechaHora_QNAME = new QName("https://siat.impuestos.gob.bo/", "sincronizarFechaHora");
    private final static QName _SincronizarFechaHoraResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "sincronizarFechaHoraResponse");
    private final static QName _SolicitudSincronizacionCodigoPuntoVenta_QNAME = new QName("", "codigoPuntoVenta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.clientesin.clientehora
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SincronizarFechaHoraResponse }
     * 
     */
    public SincronizarFechaHoraResponse createSincronizarFechaHoraResponse() {
        return new SincronizarFechaHoraResponse();
    }

    /**
     * Create an instance of {@link SincronizarFechaHora }
     * 
     */
    public SincronizarFechaHora createSincronizarFechaHora() {
        return new SincronizarFechaHora();
    }

    /**
     * Create an instance of {@link VerificarComunicacion }
     * 
     */
    public VerificarComunicacion createVerificarComunicacion() {
        return new VerificarComunicacion();
    }

    /**
     * Create an instance of {@link VerificarComunicacionResponse }
     * 
     */
    public VerificarComunicacionResponse createVerificarComunicacionResponse() {
        return new VerificarComunicacionResponse();
    }

    /**
     * Create an instance of {@link RespuestaCodigosMensajesSoapDto }
     * 
     */
    public RespuestaCodigosMensajesSoapDto createRespuestaCodigosMensajesSoapDto() {
        return new RespuestaCodigosMensajesSoapDto();
    }

    /**
     * Create an instance of {@link RespuestaFechaHora }
     * 
     */
    public RespuestaFechaHora createRespuestaFechaHora() {
        return new RespuestaFechaHora();
    }

    /**
     * Create an instance of {@link SolicitudSincronizacion }
     * 
     */
    public SolicitudSincronizacion createSolicitudSincronizacion() {
        return new SolicitudSincronizacion();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarComunicacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "verificarComunicacion")
    public JAXBElement<VerificarComunicacion> createVerificarComunicacion(VerificarComunicacion value) {
        return new JAXBElement<VerificarComunicacion>(_VerificarComunicacion_QNAME, VerificarComunicacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarComunicacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "verificarComunicacionResponse")
    public JAXBElement<VerificarComunicacionResponse> createVerificarComunicacionResponse(VerificarComunicacionResponse value) {
        return new JAXBElement<VerificarComunicacionResponse>(_VerificarComunicacionResponse_QNAME, VerificarComunicacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SincronizarFechaHora }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "sincronizarFechaHora")
    public JAXBElement<SincronizarFechaHora> createSincronizarFechaHora(SincronizarFechaHora value) {
        return new JAXBElement<SincronizarFechaHora>(_SincronizarFechaHora_QNAME, SincronizarFechaHora.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SincronizarFechaHoraResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "sincronizarFechaHoraResponse")
    public JAXBElement<SincronizarFechaHoraResponse> createSincronizarFechaHoraResponse(SincronizarFechaHoraResponse value) {
        return new JAXBElement<SincronizarFechaHoraResponse>(_SincronizarFechaHoraResponse_QNAME, SincronizarFechaHoraResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codigoPuntoVenta", scope = SolicitudSincronizacion.class)
    public JAXBElement<Integer> createSolicitudSincronizacionCodigoPuntoVenta(Integer value) {
        return new JAXBElement<Integer>(_SolicitudSincronizacionCodigoPuntoVenta_QNAME, Integer.class, SolicitudSincronizacion.class, value);
    }

}
