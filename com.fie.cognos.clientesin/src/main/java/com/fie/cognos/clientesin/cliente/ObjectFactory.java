
package com.fie.cognos.clientesin.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.clientesin.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SolicitudCuis_QNAME = new QName("https://siat.impuestos.gob.bo/", "solicitudCuis");
    private final static QName _VerificarComunicacion_QNAME = new QName("https://siat.impuestos.gob.bo/", "verificarComunicacion");
    private final static QName _VerificarComunicacionResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "verificarComunicacionResponse");
    private final static QName _SolicitudCuisResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "solicitudCuisResponse");
    private final static QName _RegistroPuntoVenta_QNAME = new QName("https://siat.impuestos.gob.bo/", "registroPuntoVenta");
    private final static QName _RegistroPuntoVentaResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "registroPuntoVentaResponse");
    private final static QName _CierreOperacionesSistema_QNAME = new QName("https://siat.impuestos.gob.bo/", "cierreOperacionesSistema");
    private final static QName _CierreOperacionesSistemaResponse_QNAME = new QName("https://siat.impuestos.gob.bo/", "cierreOperacionesSistemaResponse");
    private final static QName _SolicitudOperacionesCuisCodigoPuntoVenta_QNAME = new QName("", "codigoPuntoVenta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.clientesin.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CierreOperacionesSistema }
     * 
     */
    public CierreOperacionesSistema createCierreOperacionesSistema() {
        return new CierreOperacionesSistema();
    }

    /**
     * Create an instance of {@link CierreOperacionesSistemaResponse }
     * 
     */
    public CierreOperacionesSistemaResponse createCierreOperacionesSistemaResponse() {
        return new CierreOperacionesSistemaResponse();
    }

    /**
     * Create an instance of {@link RegistroPuntoVenta }
     * 
     */
    public RegistroPuntoVenta createRegistroPuntoVenta() {
        return new RegistroPuntoVenta();
    }

    /**
     * Create an instance of {@link RegistroPuntoVentaResponse }
     * 
     */
    public RegistroPuntoVentaResponse createRegistroPuntoVentaResponse() {
        return new RegistroPuntoVentaResponse();
    }

    /**
     * Create an instance of {@link SolicitudCuisResponse }
     * 
     */
    public SolicitudCuisResponse createSolicitudCuisResponse() {
        return new SolicitudCuisResponse();
    }

    /**
     * Create an instance of {@link VerificarComunicacion }
     * 
     */
    public VerificarComunicacion createVerificarComunicacion() {
        return new VerificarComunicacion();
    }

    /**
     * Create an instance of {@link VerificarComunicacionResponse }
     * 
     */
    public VerificarComunicacionResponse createVerificarComunicacionResponse() {
        return new VerificarComunicacionResponse();
    }

    /**
     * Create an instance of {@link SolicitudCuis }
     * 
     */
    public SolicitudCuis createSolicitudCuis() {
        return new SolicitudCuis();
    }

    /**
     * Create an instance of {@link SolicitudRegistroPuntoVenta }
     * 
     */
    public SolicitudRegistroPuntoVenta createSolicitudRegistroPuntoVenta() {
        return new SolicitudRegistroPuntoVenta();
    }

    /**
     * Create an instance of {@link RespuestaCierreSistemas }
     * 
     */
    public RespuestaCierreSistemas createRespuestaCierreSistemas() {
        return new RespuestaCierreSistemas();
    }

    /**
     * Create an instance of {@link SolicitudOperacionesCuis }
     * 
     */
    public SolicitudOperacionesCuis createSolicitudOperacionesCuis() {
        return new SolicitudOperacionesCuis();
    }

    /**
     * Create an instance of {@link RespuestaRegistroPuntoVenta }
     * 
     */
    public RespuestaRegistroPuntoVenta createRespuestaRegistroPuntoVenta() {
        return new RespuestaRegistroPuntoVenta();
    }

    /**
     * Create an instance of {@link RespuestaCodigosMensajesSoapDto }
     * 
     */
    public RespuestaCodigosMensajesSoapDto createRespuestaCodigosMensajesSoapDto() {
        return new RespuestaCodigosMensajesSoapDto();
    }

    /**
     * Create an instance of {@link SolicitudOperaciones }
     * 
     */
    public SolicitudOperaciones createSolicitudOperaciones() {
        return new SolicitudOperaciones();
    }

    /**
     * Create an instance of {@link RespuestaCuis }
     * 
     */
    public RespuestaCuis createRespuestaCuis() {
        return new RespuestaCuis();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitudCuis }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "solicitudCuis")
    public JAXBElement<SolicitudCuis> createSolicitudCuis(SolicitudCuis value) {
        return new JAXBElement<SolicitudCuis>(_SolicitudCuis_QNAME, SolicitudCuis.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarComunicacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "verificarComunicacion")
    public JAXBElement<VerificarComunicacion> createVerificarComunicacion(VerificarComunicacion value) {
        return new JAXBElement<VerificarComunicacion>(_VerificarComunicacion_QNAME, VerificarComunicacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarComunicacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "verificarComunicacionResponse")
    public JAXBElement<VerificarComunicacionResponse> createVerificarComunicacionResponse(VerificarComunicacionResponse value) {
        return new JAXBElement<VerificarComunicacionResponse>(_VerificarComunicacionResponse_QNAME, VerificarComunicacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitudCuisResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "solicitudCuisResponse")
    public JAXBElement<SolicitudCuisResponse> createSolicitudCuisResponse(SolicitudCuisResponse value) {
        return new JAXBElement<SolicitudCuisResponse>(_SolicitudCuisResponse_QNAME, SolicitudCuisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistroPuntoVenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "registroPuntoVenta")
    public JAXBElement<RegistroPuntoVenta> createRegistroPuntoVenta(RegistroPuntoVenta value) {
        return new JAXBElement<RegistroPuntoVenta>(_RegistroPuntoVenta_QNAME, RegistroPuntoVenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistroPuntoVentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "registroPuntoVentaResponse")
    public JAXBElement<RegistroPuntoVentaResponse> createRegistroPuntoVentaResponse(RegistroPuntoVentaResponse value) {
        return new JAXBElement<RegistroPuntoVentaResponse>(_RegistroPuntoVentaResponse_QNAME, RegistroPuntoVentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CierreOperacionesSistema }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "cierreOperacionesSistema")
    public JAXBElement<CierreOperacionesSistema> createCierreOperacionesSistema(CierreOperacionesSistema value) {
        return new JAXBElement<CierreOperacionesSistema>(_CierreOperacionesSistema_QNAME, CierreOperacionesSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CierreOperacionesSistemaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://siat.impuestos.gob.bo/", name = "cierreOperacionesSistemaResponse")
    public JAXBElement<CierreOperacionesSistemaResponse> createCierreOperacionesSistemaResponse(CierreOperacionesSistemaResponse value) {
        return new JAXBElement<CierreOperacionesSistemaResponse>(_CierreOperacionesSistemaResponse_QNAME, CierreOperacionesSistemaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codigoPuntoVenta", scope = SolicitudOperacionesCuis.class)
    public JAXBElement<Integer> createSolicitudOperacionesCuisCodigoPuntoVenta(Integer value) {
        return new JAXBElement<Integer>(_SolicitudOperacionesCuisCodigoPuntoVenta_QNAME, Integer.class, SolicitudOperacionesCuis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codigoPuntoVenta", scope = SolicitudOperaciones.class)
    public JAXBElement<Integer> createSolicitudOperacionesCodigoPuntoVenta(Integer value) {
        return new JAXBElement<Integer>(_SolicitudOperacionesCuisCodigoPuntoVenta_QNAME, Integer.class, SolicitudOperaciones.class, value);
    }

}
