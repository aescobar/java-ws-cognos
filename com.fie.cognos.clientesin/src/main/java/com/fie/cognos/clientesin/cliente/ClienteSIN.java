package com.fie.cognos.clientesin.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class ClienteSIN {

	public static void main(String[] args) {
		ServicioFacturacionOperacionesService sfos = new ServicioFacturacionOperacionesService();
		IServicioFacturacionOperaciones cliente = sfos.getServicioFacturacionOperacionesPort();
		int resp = cliente.verificarComunicacion();
		System.out.println(resp);
		
		SolicitudOperacionesCuis solCuis = new SolicitudOperacionesCuis();
		solCuis.setCodigoAmbiente(1);
		solCuis.setCodigoModalidad(1);
		solCuis.setCodigoSistema("prueba");
		solCuis.setNit(123);
		//solCuis.setCodigoPuntoVenta(new JAXBElement<Integer>(new QName("xxxxxx", "codigoPuntoVenta"), Integer.class, SolicitudOperacionesCuis.class, 1));
		ObjectFactory objectFactory = new ObjectFactory();
		solCuis.setCodigoPuntoVenta(objectFactory.createSolicitudOperacionesCodigoPuntoVenta(1));
		RespuestaCuis respCuis =  cliente.solicitudCuis(solCuis);
		
		System.out.println(respCuis.toString());
		
		
	
	}
}
