
package com.fie.cognos.clientesin.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para solicitudCuis complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="solicitudCuis">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SolicitudCuis" type="{https://siat.impuestos.gob.bo/}solicitudOperacionesCuis"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "solicitudCuis", propOrder = {
    "solicitudCuis"
})
public class SolicitudCuis {

    @XmlElement(name = "SolicitudCuis", required = true)
    protected SolicitudOperacionesCuis solicitudCuis;

    /**
     * Obtiene el valor de la propiedad solicitudCuis.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudOperacionesCuis }
     *     
     */
    public SolicitudOperacionesCuis getSolicitudCuis() {
        return solicitudCuis;
    }

    /**
     * Define el valor de la propiedad solicitudCuis.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudOperacionesCuis }
     *     
     */
    public void setSolicitudCuis(SolicitudOperacionesCuis value) {
        this.solicitudCuis = value;
    }

}
