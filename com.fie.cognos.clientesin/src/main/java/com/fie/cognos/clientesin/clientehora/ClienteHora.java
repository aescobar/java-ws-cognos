package com.fie.cognos.clientesin.clientehora;

public class ClienteHora {
	
	public static void main(String[] args) {
		ServicioSincronizacionFechaHoraService sshora = new ServicioSincronizacionFechaHoraService();
		IServicioSincronizacionFechaHora cliente = sshora.getServicioSincronizacionFechaHoraPort();
		System.out.println(cliente.verificarComunicacion());
	}

}
