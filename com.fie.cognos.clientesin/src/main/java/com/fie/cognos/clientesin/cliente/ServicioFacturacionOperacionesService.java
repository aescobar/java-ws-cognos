
package com.fie.cognos.clientesin.cliente;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ServicioFacturacionOperacionesService", targetNamespace = "https://siat.impuestos.gob.bo/FacturacionOperaciones", wsdlLocation = "https://presiatservicios.impuestos.gob.bo:39117/FacturacionOperaciones?wsdl")
public class ServicioFacturacionOperacionesService
    extends Service
{

    private final static URL SERVICIOFACTURACIONOPERACIONESSERVICE_WSDL_LOCATION;
    private final static WebServiceException SERVICIOFACTURACIONOPERACIONESSERVICE_EXCEPTION;
    private final static QName SERVICIOFACTURACIONOPERACIONESSERVICE_QNAME = new QName("https://siat.impuestos.gob.bo/FacturacionOperaciones", "ServicioFacturacionOperacionesService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("https://presiatservicios.impuestos.gob.bo:39117/FacturacionOperaciones?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SERVICIOFACTURACIONOPERACIONESSERVICE_WSDL_LOCATION = url;
        SERVICIOFACTURACIONOPERACIONESSERVICE_EXCEPTION = e;
    }

    public ServicioFacturacionOperacionesService() {
        super(__getWsdlLocation(), SERVICIOFACTURACIONOPERACIONESSERVICE_QNAME);
    }

    public ServicioFacturacionOperacionesService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SERVICIOFACTURACIONOPERACIONESSERVICE_QNAME, features);
    }

    public ServicioFacturacionOperacionesService(URL wsdlLocation) {
        super(wsdlLocation, SERVICIOFACTURACIONOPERACIONESSERVICE_QNAME);
    }

    public ServicioFacturacionOperacionesService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICIOFACTURACIONOPERACIONESSERVICE_QNAME, features);
    }

    public ServicioFacturacionOperacionesService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ServicioFacturacionOperacionesService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IServicioFacturacionOperaciones
     */
    @WebEndpoint(name = "ServicioFacturacionOperacionesPort")
    public IServicioFacturacionOperaciones getServicioFacturacionOperacionesPort() {
        return super.getPort(new QName("https://siat.impuestos.gob.bo/FacturacionOperaciones", "ServicioFacturacionOperacionesPort"), IServicioFacturacionOperaciones.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IServicioFacturacionOperaciones
     */
    @WebEndpoint(name = "ServicioFacturacionOperacionesPort")
    public IServicioFacturacionOperaciones getServicioFacturacionOperacionesPort(WebServiceFeature... features) {
        return super.getPort(new QName("https://siat.impuestos.gob.bo/FacturacionOperaciones", "ServicioFacturacionOperacionesPort"), IServicioFacturacionOperaciones.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SERVICIOFACTURACIONOPERACIONESSERVICE_EXCEPTION!= null) {
            throw SERVICIOFACTURACIONOPERACIONESSERVICE_EXCEPTION;
        }
        return SERVICIOFACTURACIONOPERACIONESSERVICE_WSDL_LOCATION;
    }

}
