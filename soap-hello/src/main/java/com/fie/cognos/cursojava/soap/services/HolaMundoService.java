package com.fie.cognos.cursojava.soap.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.fie.cognos.cursojava.soap.entities.Persona;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class HolaMundoService {

	@Resource
	WebServiceContext wsc;

	@WebMethod
	public String saludar(String nombre) {
		MessageContext messageContext = wsc.getMessageContext();
		Map<String, Object> headers = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		List<String> usuario = (List<String>) headers.get("usuario");
		List<String> password = (List<String>) headers.get("password");
		if (usuario != null && password != null) {
			if (usuario.get(0).equals("ariel") && password.get(0).equals("patito")) {
				return "Bienvenido ariel";
			}
		}
		return "Error de autenticacion ";
	}

	@WebMethod(operationName = "cargarPersona")
	@WebResult(name = "persona")
	public Persona cargarPersona(@WebParam(name = "nombre") String nombre, @WebParam(name = "paterno") String paterno,
			@WebParam(name = "materno") String materno, @WebParam(name = "edad") int edad) {
		return new Persona(nombre, paterno, materno, edad);
	}

	@WebMethod
	public ArrayList<Persona> listarPeronas() {
		Persona p1 = new Persona("nombre1", "paterno1", "materno1", 1);
		Persona p2 = new Persona("nombre2", "paterno2", "materno1", 1);
		Persona p3 = new Persona("nombre3", "paterno3", "materno1", 1);
		Persona p4 = new Persona("nombre4", "paterno4", "materno1", 1);
		Persona p5 = new Persona("nombre5", "paterno5", "materno1", 1);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(p1);
		personas.add(p2);
		personas.add(p3);
		personas.add(p4);
		personas.add(p5);
		return personas;

	}

}
