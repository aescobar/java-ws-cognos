package com.fie.cognos.cursojava.soap.client;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;



public class ClienteHolaMundo {

	public static void main(String[] args) {
		
		
		HolaMundoServiceService service = new HolaMundoServiceService();
		HolaMundoService cliente = service.getHolaMundoServicePort();
		Map<String, Object> mapRequests = ((BindingProvider)cliente).getRequestContext();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		headers.put("usuario", Collections.singletonList("ariel"));
		headers.put("pasword", Collections.singletonList("patito"));
		mapRequests.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
		System.out.println(cliente.saludar("Ariel"));
		
		Persona persona =  cliente.cargarPersona("Ariel", "Escobar", "Endara", 12);
		System.out.println("DATOS PERSONA");
		System.out.println("NOMBRE: " + persona.getNombre());
		System.out.println("PATERNO: " + persona.getPaterno());
		System.out.println("MATERNO: " + persona.getMaterno());
		System.out.println("EDAD: " + persona.getEdad());
		
		List<Persona> persoas = cliente.listarPeronas();
		for(Persona personaItem:persoas) {
			
			System.out.println(personaItem.nombre);
			
		}
		
		
	}

}
