package com.fie.cognos.rest.restpersona.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fie.cognos.rest.restpersona.db.PersonaDAO;
import com.fie.cognos.rest.restpersona.entities.Persona;
import com.fie.cognos.rest.restpersona.entities.PojoProc;
import com.fie.cognos.rest.restpersona.entities.RespuestaServicio;

@Path("personas")
public class PersonaRest {

	private PersonaDAO personaDAO = new PersonaDAO();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listPersonas() {
		List<Persona> personas = personaDAO.listPersonas();
		if (personas != null && !personas.isEmpty()) {
			System.out.println("Correcto!!!!!");
			return Response.ok().entity(personas).build();
		} else {
			System.out.println("Error no existe datos");
			return Response.ok().entity(new RespuestaServicio("001", "No existen registros")).build();
		}
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersona(@PathParam("id") long id) {
		Persona persona = personaDAO.getPersona(id);
		if (persona != null) {
			System.out.println("Correcto!!!!!");
			return Response.ok().entity(persona).build();
		} else {
			System.out.println("Error no existe datos");
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response inserPersona(Persona persona) {
		boolean resp = personaDAO.insertPersona(persona);
		if (resp) {
			System.out.println("Correcto!!!!!");
			return Response.ok().build();
		} else {
			System.out.println("Error no existe datos");
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	
	@POST
	@Path("/form")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response inserPersona(@FormParam("nombre") String nombre, 
			@FormParam("paterno") String paterno,
			 @FormParam("materno") String materno, @FormParam("edad") int edad) {
		System.out.println("Insert con FormParam......");
		Persona persona = new Persona();
		persona.setNombre(nombre);
		persona.setPaterno(paterno);
		persona.setMaterno(materno);
		persona.setEdad(edad);
		boolean resp = personaDAO.insertPersona(persona);
		if (resp) {
			System.out.println("Correcto!!!!!");
			return Response.ok().build();
		} else {
			System.out.println("Error no existe datos");
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePersona(Persona persona) {
		if (persona.getId() != 0) {
			boolean resp = personaDAO.updatePersona(persona);
			if (resp) {
				System.out.println("Correcto!!!!!");
				return Response.ok().build();
			} else {
				System.out.println("Error no existe datos");
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}else {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@GET
	@Path("/procedure/{x}/{y}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response callProcedure(@PathParam("x") int x, @PathParam("y") int y) {
		PojoProc pojoProc = personaDAO.callSumaProd(x, y);
		if (pojoProc != null) {
			System.out.println("Correcto Procedimiento!!!!!");
			return Response.ok().entity(pojoProc).build();
		} else {
			System.out.println("Error no se pudo llamar al procedimiento");
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@GET
	@Path("/byedad/{edad}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listPersonasByEdad(@PathParam("edad") int edad) {
		List<Persona> personas = personaDAO.listPersonasByEdad(edad);
		if (personas != null) {
			System.out.println("Correcto Procedimiento!!!!!");
			return Response.ok().entity(personas).build();
		} else {
			System.out.println("Error no se pudo llamar al procedimiento");
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}
