package com.fie.cognos.rest.restpersona.db;

import java.util.List;

import com.fie.cognos.rest.restpersona.entities.PojoProc;
import org.apache.ibatis.session.SqlSession;

import com.fie.cognos.rest.restpersona.entities.Persona;

public class PersonaDAO {

    public List<Persona> listPersonas() {
        SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
        PersonaMapper mapper = session.getMapper(PersonaMapper.class);
        List<Persona> personas = mapper.listPersonas();
        session.close();
        return personas;
    }

    public Persona getPersona(long id) {
        SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
        PersonaMapper mapper = session.getMapper(PersonaMapper.class);
        Persona persona = mapper.getPersona(id);
        session.close();
        return persona;
    }

    public boolean insertPersona(Persona persona) {
        try {
            SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
            PersonaMapper mapper = session.getMapper(PersonaMapper.class);
            mapper.insertPersona(persona);
            session.commit();
            session.close();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean updatePersona(Persona persona) {
        try {
            SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
            PersonaMapper mapper = session.getMapper(PersonaMapper.class);
            mapper.updatePersona(persona);
            session.commit();
            session.close();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public PojoProc callSumaProd(int x, int y) {
        try {
            SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
            PersonaMapper mapper = session.getMapper(PersonaMapper.class);
            PojoProc pojo  = mapper.callSumaProd(x,y);
            session.close();
            return pojo;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
    public List<Persona> listPersonasByEdad(int edad) {
        try {
            SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
            PersonaMapper mapper = session.getMapper(PersonaMapper.class);
            List<Persona> personas  = mapper.listPersonasByEdad(edad);
            session.close();
            return personas;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }


}
