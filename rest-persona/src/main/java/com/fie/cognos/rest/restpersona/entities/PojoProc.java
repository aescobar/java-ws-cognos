/******************************
 * Banco Central De Bolivia
 * La Paz - Bolivia
 * com.fie.cognos.rest.restpersona.entities 
 * rest-persona 
 * 02-09-19 - 10:19 AM
 * Creado por aescobar
 ******************************/
package com.fie.cognos.rest.restpersona.entities;

/*
 *
 */
public class PojoProc {

    int suma;
    int producto;


    public PojoProc() {
    }



    public PojoProc(int suma, int producto) {
        this.suma = suma;
        this.producto = producto;

    }

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }

}
