package com.fie.cognos.rest.restpersona.entities;

public class RespuestaServicio {
	
	private String codRespuesta;
	private String descripcion;
	public RespuestaServicio(String codRespuesta, String descripcion) {
		super();
		this.codRespuesta = codRespuesta;
		this.descripcion = descripcion;
	}
	public String getCodRespuesta() {
		return codRespuesta;
	}
	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	

}
