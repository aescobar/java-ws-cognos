package com.fie.cognos.rest.restpersona.db;

import java.util.List;

import com.fie.cognos.rest.restpersona.entities.PojoProc;
import org.apache.ibatis.annotations.*;

import com.fie.cognos.rest.restpersona.entities.Persona;
import org.apache.ibatis.mapping.StatementType;

public interface PersonaMapper {

	@Select("SELECT * FROM persona2")
	List<Persona> listPersonas();
	
	@Select("SELECT * FROM persona2 WHERE id =#{id}")
	Persona getPersona(long id);
	
	@Insert("INSERT INTO persona2(nombre, paterno,materno,edad) values(#{nombre},#{paterno}, #{materno},#{edad}) ")
	void insertPersona(Persona persona);
	
	@Update("UPDATE persona2 SET nombre =#{nombre}, paterno =#{paterno}, materno=#{materno}, edad=#{edad} WHERE id =#{id} ")
	void updatePersona(Persona persona);
	
//EJemplo de consumo de procedimiento inout
//	CREATE OR REPLACE FUNCTION sum_prod(IN dato1 int,IN dato2 int, OUT suma int, OUT producto int) AS $$
//	BEGIN
//	    suma := dato1 + dato2;
//	    producto := dato1 * dato2;
//	END;
//	$$ LANGUAGE plpgsql
	@Select("SELECT * FROM  sum_prod(#{dato1},#{dato2})")
	@ResultType(PojoProc.class)
	PojoProc callSumaProd(@Param("dato1") int dato1, @Param("dato2") int dato2);

//	CREATE OR REPLACE FUNCTION list_persona_edad (p_edad INT) 
//	   RETURNS TABLE (
//	      nombre VARCHAR,
//	      paterno VARCHAR,
//	      materno VARCHAR
//	) AS $$
//	DECLARE 
//	    var_r record;
//	BEGIN
//	   FOR var_r IN(SELECT * FROM persona WHERE  edad = p_edad)  
//	   LOOP
//	        nombre := upper(var_r.nombre) ; 
//	        paterno := var_r.paterno;
//	        materno := var_r.materno;
//	        RETURN NEXT;
//	   END LOOP;
//	END; $$ 
//	LANGUAGE 'plpgsql';	
	@Select("select * from list_persona_edad(#{edad})")
	@ResultType(Persona.class)
	List<Persona> listPersonasByEdad(@Param("edad") int edad);
	
	
}
