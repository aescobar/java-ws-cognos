package com.fiue.cognos.rest.services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class AppConfig extends Application {
	
	Set<Object> singletons = new HashSet<Object>();
	
	public AppConfig() {
		singletons.add(new PersonaService());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
	
	
}
