package com.fie.cognos.rest.jboss.restpersona_cliente.reseasy;

import java.io.IOException;
import java.util.Base64;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.ext.Provider;

//@Provider
public class ClientePersonaFilter implements ClientRequestFilter, ClientResponseFilter {

	public void filter(ClientRequestContext requestContext) throws IOException {
		String credenciales = "ariel:prueba";
		byte[] credencialesEncode = Base64.getEncoder().encode(credenciales.getBytes());
		System.out.println(credenciales);
		requestContext.getHeaders().add("Authorization", "Basic " + new String(credencialesEncode));
		System.out.println(new String(credencialesEncode));
	}

	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
