package com.fie.cognos.rest.jboss.restpersona_cliente.reseasy;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fie.cognos.rest.jboss.restpersona_cliente.entities.Persona;


@Path("personas")
public interface PersonaCliente {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listPersonas();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersona(@PathParam("id") long id);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response inserPersona(Persona persona);
	
	@POST
	@Path("/form")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response inserPersona(@FormParam("nombre") String nombre, 
			@FormParam("paterno") String paterno,
			 @FormParam("materno") String materno, @FormParam("edad") int edad);
	

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePersona(Persona persona);

}
