package com.fie.cognos.rest.jboss.restpersona_cliente.reseasy;

import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fie.cognos.rest.jboss.restpersona_cliente.entities.Persona;
import com.fie.cognos.rest.jboss.restpersona_cliente.entities.RespuestaServicio;

public class ClienteRestEasy {

	public static void main(String[] args) {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		cliente.register(ClientePersonaFilter.class);
		ResteasyWebTarget target = cliente.target(UriBuilder.fromPath("http://localhost:8080/rest-persona/rest/"));
		PersonaCliente personaCliente = target.proxy(PersonaCliente.class);

		Response response = personaCliente.getPersona(1L);

		Persona persona = response.readEntity(Persona.class);

		System.out.println(persona.toString());

		Response responseListado = personaCliente.listPersonas();

		if (response.getStatus() == 200) {
			try {
				List<Persona> personas = responseListado.readEntity(new GenericType<List<Persona>>() {
				});
				for (Persona item : personas) {
					System.out.println(item);
				}
			} catch (Exception e) {
				RespuestaServicio resp = responseListado.readEntity(RespuestaServicio.class);
				System.out.println(resp.getCodRespuesta() + " - " + resp.getDescripcion());
			}
		} else {
			System.out.println("Error al consumir servicio rest");
		}
	}

}
