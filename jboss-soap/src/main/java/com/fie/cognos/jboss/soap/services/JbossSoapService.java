package com.fie.cognos.jboss.soap.services;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService(endpointInterface = "com.fie.cognos.jboss.soap.services.IJbossSoapService")
@HandlerChain(file = "handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class JbossSoapService implements IJbossSoapService {
	
	@WebMethod
	public String saludar(String nombre) {
		return "Hola " + nombre;
	}

	public String saludarSinWebMethod(String nombre) {
		return "Hola " + nombre;
	}
	
	private String saludarPrivado(String nombre) {
		return "Hola " + nombre;
	}
	
	
}
