package com.fie.cognos.jboss.soap.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ValidationHandler implements SOAPHandler<SOAPMessageContext>{

	public boolean handleMessage(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleMessage .........");
		mostrarMensajes(context);
		if(validarDatos(context)) {
			return true;
		}
		return false;
	}

	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleFault .........");
		return false;
	}

	public void close(MessageContext context) {
		System.out.println("close .........");
		// TODO Auto-generated method stub
		
	}

	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		System.out.println("getHeaders .........");
		return null;
	}
	
	public void mostrarMensajes(SOAPMessageContext context) {	
		try {
			boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			if(isOutbound)
				System.out.println("Mensaje de salida.....");
			else
				System.out.println("MEnsajes de entrada.....");
			
			SOAPMessage soapMessage = context.getMessage();
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean validarDatos(SOAPMessageContext context) {
		boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if(!isOutbound) {
			System.out.println("MEnsajes de entrada.....");
			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			String ip = request.getRemoteAddr();
			System.out.println("IP: " + request.getRemoteAddr());
			System.out.println("HOST: " + request.getRemoteHost());
			System.out.println("USUARIO: " + request.getHeader("usuario"));
			if(ip.trim().equals("192.168.3.112")){
				return false;
			}
			
		}
		return true;
		
	}

}
